let studentList = [];

let studentsAdded = 0;

function addStudent(name) {
    studentList.push({ name: name, section: [] });
    console.log(`${name} is added to the student list`);
    studentsAdded++;
}

function displayTotalNumbersAdded() {
    console.log(studentsAdded);
}

function displayPrevStudentAdded() {
    if (studentList.length > 0) {
        console.log(studentList[studentList.length - 1]);
    } else {
        console.log("No Students");
    }
}

function addSection(studentName, section) {
    studentList = studentList.map((student) => {
        if (studentName === student.name) {
            return { ...student, section: [...student.section, section] };
        } else {
            return student;
        }
    });

    console.log(`Student name: ${studentName} Section: ${section}`);
}

function checkEnrollee(name) {
    const enrollee = studentList.filter((student) => {
        return student.name.toLowerCase() === name.toLowerCase();
    });

    if (enrollee.length > 0) {
        console.log(`${name} is on the list`);
        return true;
    } else {
        console.log(`${name} is not on the list`);
        return false;
    }
}

function removeStudent(name) {
    const studentExist = checkEnrollee(name);

    if (studentExist) {
        studentList = studentList.filter((student) => {
            return student.name.toLowerCase() !== name.toLowerCase();
        });

        console.log(`${name} has been removed from the student list`);
    }

    displayStudents();
}

function displayStudents() {
    for (let student of studentList) {
        console.log(student.name);
    }
}

addStudent("Regienald Almoite");

addSection("Regienald Almoite", "Section 12");

checkEnrollee("Regienald Almoite");

addStudent("Joe");
displayPrevStudentAdded();
checkEnrollee("Joe");
removeStudent("Joe");
